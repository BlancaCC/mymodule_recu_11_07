class mymodule::mysql_config{
	###mySQL server
	class { '::mysql::server': 
		root_password => 'vagrantpass'
	}
	mysql::db { 'mympwar':
		user     => 'vagrant',
		password => 'mpwardb',
	}
	mysql::db { 'mpwar_test':
		user     => 'vagrant',
		password => 'mpwardb',
	}
}